#include <cmath>
#include <random>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <eigen3/Eigen/Dense>
using namespace std;

using namespace Eigen;

template<typename T, size_t nEq, size_t nCanal>
std::tuple<T, T, T, T, Matrix<T, nEq, 1>> simula(size_t nSinal, const Matrix<T, nCanal, 1>& hCanal, int atraso, int atrasoSemEq, T SNR);

constexpr double qFunc(double x) {
    return 1 - std::erfc(-x/std::sqrt(2))/2;
}

template<typename T, size_t n1, size_t n2>
constexpr Matrix<double, n1 + n2 - 1, 1> conv(const Matrix<double, n1, 1>& v1, const Matrix<double, n2, 1>& v2) {
  Matrix<double, n1 + n2 - 1, 1> c;

  for (size_t i = 0; i < n1 + n2 - 1; i++) {
      size_t jmin, jmax;

      c(i) = 0;

      jmin = (i >= n2 - 1) ? i - (n2 - 1) : 0;
      jmax = (i < n1 - 1) ? i : n1 - 1;

      for (size_t j = jmin; j <= jmax; j++)
	c(i) += v1(j) * v2(i - j);
  }

  return std::move(c);
}

int main(){
  constexpr size_t nCanal = 4;
  constexpr size_t nEq = 10;
  constexpr size_t nPulso = 2*nEq - 1;
  constexpr size_t nSinal = 1e6;

  //vetores com a resposta ao impulso invertida, i.e. {h_3, h_2, h_1, h_0} (para convolucao ser ser feita como produto escalar)
  std::array<Matrix<double, nCanal, 1>, 3> h;
  h[0] = {0.1173, 0.4499, 0.8576, 0.2199};
  h[1] = {0.0352, 0.2417, 0.6347, 0.7332};
  h[2] = {0.7332, 0.6347, 0.2417, 0.0352};

  std::fstream file;
  
  //Item c - MSE x atraso
  constexpr std::array<double, 3> SNR = {1, pow(10, 0.9), pow(10, 1.8)};
  
  for(size_t i = 0; i < 3; i++)
    for(size_t j = 0; j < 3; j++) {
      file.open("MSE_SNR_" + std::to_string(10*log10(SNR[i])) + "_h" + std::to_string(j + 1) + ".csv", fstream::out);
      for(int atraso = 0; atraso < nEq; atraso++) {
	double BER, MSE, BERsEq, MSEsEq;
	Matrix<double, nEq, 1> hEq;
	std::tie(BER, MSE, BERsEq, MSEsEq, hEq) = simula<double, nEq, nCanal>(nSinal, h[j], atraso, 0, SNR[i]);
	file << atraso << ", " << 10*log10(MSE) << std::endl;
      }
      file.close();
    }

  //Itens D e E - BER x SNR (para h1)
  std::fstream file2;
  file.open("BERxSNR_h1.csv", fstream::out);
  file2.open("BERxSNR_h1_analitico.csv", fstream::out);
  for(float SNRi = 3e-5; SNRi <= pow(10, 1.8); SNRi *= pow(10, 0.2)) {
    float BER, MSE, BERsEq, MSEsEq;
    Matrix<double, nEq, 1> hEq;
    std::tie(BER, MSE, BERsEq, MSEsEq, hEq) = simula<double, nEq, nCanal>(nSinal, h[0], 6, 1, SNRi);

    //calcula resposta conjunta do canal (canal + equalizador)
    Matrix<double, nEq + nCanal - 1, 1> s;
    s = conv<double, nCanal, nEq>(h[0], hEq);
    
    file << 10*log10(SNRi) << ", " << BER << ", " << BERsEq << std::endl;
    file2 << 10*log10(SNRi) << ", " << qFunc(s(s.rows() - 1 - 6)/sqrt(s.squaredNorm() - s(s.rows() - 1 - 6)*s(s.rows() - 1 - 6) + hEq.squaredNorm()/SNRi)) << std::endl;
  }

  return 0;
}

template<typename T, size_t nEq, size_t nCanal>
std::tuple<T, T, T, T, Matrix<T, nEq, 1>> simula(size_t nSinal, const Matrix<T, nCanal, 1>& hCanal, int atraso, int atrasoSemEq, T SNR) {
  //sinal de entrada
  Matrix<T, Dynamic, 1> u(nSinal);
  
  std::random_device rd;
  std::mt19937 gen(rd());
  std::bernoulli_distribution bernDist(0.5);
  std::normal_distribution<T> noise(0, sqrt(1/SNR));

  //Gera sinal 2-PAM (potencia unitária)
  for(int i = 0; i < u.rows(); i++)
    u(i) = 2.0*bernDist(gen) - 1.0;

  //Cálculo dos coeficientes do equalizador pelo criterio MMSE
  Matrix<T, nEq, 1> hEq;

  Matrix<T, nCanal + nEq - 1, nEq> H = Matrix<T, nCanal + nEq - 1, nEq>::Zero();

  for(int i = 0; i < nEq; i++)
    H.template block<nCanal, 1>(i, i) = hCanal;

  Matrix<T, nCanal + nEq - 1, 1> v = Matrix<T, nCanal + nEq - 1, 1>::Zero();
  v(v.rows() - atraso - 1) = 1;
  
  hEq = (H.adjoint()*H + (1/SNR)*Matrix<T, nEq, nEq>::Identity()).colPivHouseholderQr().solve(H.adjoint()*v);

  // std::cout << "EQ:" << std::endl << hEq << std::endl;

  //Calcula saída do canal e estima MSE sem equalizacao
  float MSEsemEq = 0.0;
  
  Matrix<T, Dynamic, 1> v1(u.rows() - nCanal + 1);
  for(int i = 0; i < v1.rows(); i++)
    v1(i) = hCanal.dot(u.template segment<nCanal>(i)) + noise(gen);

  //Estima MSE sem equalização
  for(int i = 0; i < v1.rows(); i++) {
    auto err = v1(i) - u(i + nCanal - 1 - atrasoSemEq);
    MSEsemEq += err*err/v1.rows();
  }
  
  //Calcula Saida do Equalizador e estima MSE
  Matrix<T, Dynamic, 1> v2(v1.rows() - nEq + 1);
  float MSE = 0.0;
  
  for(int i = 0; i < v2.rows(); i++) {
    v2(i) = hEq.dot(v1.template segment<nEq>(i));
    auto err = v2(i) - u(i + nEq - 1 + nCanal - 1 - atraso);
    MSE += err*err/v2.rows();
  }
    
  //Calcula simbulos detectados
  Matrix<T, Dynamic, 1> aOut(v2.rows());
  
  for(int i = 0; i < v2.rows(); i++)
    aOut(i) = (v2(i) >= 0) - (v2(i) < 0);

  Matrix<T, Dynamic, 1> aOutSemEq(v1.rows());
  
  for(int i = 0; i < v1.rows(); i++)
    aOutSemEq(i) = (v1(i) >= 0) - (v1(i) < 0);
    
  //Contagem dos erros
  int nErr = 0;
  
  for(int i = 0; i < aOut.rows(); i++)
    if(aOut(i) != u(i + nEq - 1 + nCanal - 1 - atraso))
      nErr++;

  int nErrSemEq = 0;
  for(int i = 0; i < aOutSemEq.rows(); i++)
    if(aOutSemEq(i) != u(i + nCanal - 1 - atrasoSemEq))
      nErrSemEq++;  
  
  return std::make_tuple(T(nErr)/aOut.rows(), MSE, T(nErrSemEq)/aOutSemEq.rows(), MSEsemEq, std::move(hEq));
}
