figure; X = csvread("MSE_SNR_0.000000_h1.csv"); hold on; plot(X(:, 1), X(:, 2)); X = csvread("MSE_SNR_9.000000_h1.csv"); plot(X(:, 1), X(:, 2)); X = csvread("MSE_SNR_18.000000_h1.csv"); plot(X(:, 1), X(:, 2));
ylabel("MSE [dB]");
xlabel("Atraso");
title("MSE vs atraso com canal h1 equalizado");
legend({"SNR = 0 dB", "SNR = 9 dB", "SNR = 18 dB"}, "Location", "Eastoutside")

print -dpng itemCh1.png

figure; X = csvread("MSE_SNR_0.000000_h2.csv"); hold on; plot(X(:, 1), X(:, 2)); X = csvread("MSE_SNR_9.000000_h2.csv"); plot(X(:, 1), X(:, 2)); X = csvread("MSE_SNR_18.000000_h2.csv"); plot(X(:, 1), X(:, 2));
ylabel("MSE [dB]");
xlabel("Atraso");
title("MSE vs atraso com canal h2 equalizado");
legend({"SNR = 0 dB", "SNR = 9 dB", "SNR = 18 dB"}, "Location", "Eastoutside")

print -dpng itemCh2.png

figure; X = csvread("MSE_SNR_0.000000_h3.csv"); hold on; plot(X(:, 1), X(:, 2)); X = csvread("MSE_SNR_9.000000_h3.csv"); plot(X(:, 1), X(:, 2)); X = csvread("MSE_SNR_18.000000_h3.csv"); plot(X(:, 1), X(:, 2));
ylabel("MSE [dB]");
xlabel("Atraso");
title("MSE vs atraso com canal h3 equalizado");
legend({"SNR = 0 dB", "SNR = 9 dB", "SNR = 18 dB"}, "Location", "Eastoutside")

print -dpng itemCh3.png

figure; X = csvread("BERxSNR_h1.csv"); semilogy(X(:, 1), X(:, 2), X(:, 1), X(:, 3));

xlabel("SNR [dB]");
ylabel("BER");
title("BER vs SNR para o canal h1");
legend({"Com Equalizador (atraso = 6)", "Sem Equalizador (atraso = 1)"}, "Location", "southEast");

print -dpng itemD.png

figure; Y = csvread("BERxSNR_h1_analitico.csv"); semilogy(X(:, 1), X(:, 2), "linewidth", 1.0, Y(:, 1), Y(:, 2), "linestyle", "--", "linewidth", 2.0);

xlabel("SNR [dB]");
ylabel("BER");
title("BER vs SNR para o canal h1");
legend("Simulado", "Teorico");

print -dpng itemE.png
